<?php
function actionArea(){
    $lados=$_REQUEST["lados"];
    $p= array_sum($lados)/2;
    $radical=$p*($p-$lados[0])*($p-$lados[1])*($p-$lados[2]);
    if($radical>0){
        $mensaje="El área del rectángulo es: ";
    $area=sqrt($radical);
        }else{
            $mensaje="Los lados no forman un triángulo";
            $area="";
        }
    return [
        "content"=>"area.php",
        "mensaje"=>$mensaje,
        "resultado"=>$area,
        "pie" => "I. Garrote",
    ];
}
function actionIndex(){
    return[
		"titulo" => "Introduce los datos",
        "content"=>"formulario.php",
		"pie" => "I. Garrote",
    ];
}

if(isset($_REQUEST["calcular"])){
    $datos= actionArea();
}else{
    $datos= actionIndex();
}
