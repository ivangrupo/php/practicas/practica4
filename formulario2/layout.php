<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Layout</title>
    </head>
    <body>
        <h1><?= $datos["titulo"]; ?></h1>
        <?php
            include $datos["content"];
        ?>
        <footer>
            <h3><?= $datos["pie"]; ?></h3>
        </footer>
    </body>
</html>

