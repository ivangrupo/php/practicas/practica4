<?php

    if (isset($_REQUEST["imprimir"])) {
        $resultado="";
        $numeros = $_REQUEST["numero"];

        $ord_numeros = $numeros;
        sort($ord_numeros);

        foreach ($ord_numeros as $valores) {
            $resultado.="$valores ";
        }

        if ($ord_numeros == $numeros) {
            $resultado.=": Los datos estaban ordenados";
        } else {
            $resultado.=": Los datos no estaban ordenados";
        }
        
        $datos=[
            "titulo" => "Los números ordenados ascendentemente son:",
            "content" => "resultado.php",
            "mensaje" => "Prueba de nuevo",
            "resultado" => $resultado,
            "pie" => "I. Garrote",
        ];
    }else{
        $datos=[
            "titulo" => "Introduce los datos",
            "content" => "formulario.php",
            "pie" => "I. Garrote",
        ];
    } 
