<!DOCTYPE html>

<html>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title><?= $datos["titulo"]; ?></title>
  </head>
  <style>
    .container{
        display: block;
        text-align: center;
        background-color: #ccc;
    }
    div, label, imput, button{
        margin: 5px;
        text-decoration: none;
    }
    h3{
        margin: 20px 100px;
    }
  </style>
  <body>
    <div class="container">
        
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg navbar-dark bg-danger text-danger">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav mr-auto">
                        <li class="nav-item <?= ($datos["content"] == 0) ? "active" : ""; ?>">
                          <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item <?= ($datos["content"] == 1) ? "active" : ""; ?>">
                          <a class="nav-link" href="index.php?view=1">Media Aritmética</a>
                        </li>
                        <li class="nav-item <?= ($datos["content"] == 2) ? "active" : ""; ?>">
                          <a class="nav-link" href="index.php?view=2">Moda</a>
                        </li>
                        <li class="nav-item <?= ($datos["content"] == 3) ? "active" : ""; ?>">
                          <a class="nav-link" href="index.php?view=3">Mediana</a>
                        </li>
                        <li class="nav-item <?= ($datos["content"] == 4) ? "active" : ""; ?>">
                          <a class="nav-link" href="index.php?view=4">Desviación Tipica</a>
                        </li>
                      </ul>
                    </div>
                </nav>
          </div>

        <div class="container mt-3">
            <h1><?= $datos["titulo"]; ?></h1>
            
            <div class="container mt-2">
                <h3><a href="index.php" class="text-danger">FORMULARIO</a></h3>
            </div>
            
            <form method="get">
                <?php
                include $datos["content"];
                ?>
            </form>
        </div>

        <div class="card mt-5 bg-danger">
            <div class="card-body mx-auto">
                <h4><?= $datos["pie"]; ?></h4>
            </div>
        </div>
        
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>


