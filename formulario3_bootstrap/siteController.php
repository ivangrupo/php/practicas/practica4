<?php

function actionIndex() {
    return [
        "titulo" => "Operaciones matemáticas",
        "content" => "formulario.php",
        "pie" => "© 2018 CURSO DE DESARROLLO DE APLICACIONES CON TECNOLOGÍA WEB &nbsp; - &nbsp; I. Garrote",
    ];
}

function actionMedia() {
    $datos = $_REQUEST['numeros'];
    /*
      Comprobar si es array o string
     */
    if (gettype($datos) == "string") {
        $datos = explode(",", $datos);
        echo var_dump($datos);
    }
    $resultado = 0;
    foreach ($datos as $v) {
        $resultado += $v;
    }
    $resultado /= count($datos);
    /*
      Convertir los nº en un string
     */
    $datos = implode(",", $datos);
    echo var_dump($datos);
    return [
        'titulo' => "Operaciones matemáticas",
        'content' => 'resultado.php',
        'mensaje' => 'La media es: ',
        "pie" => "© 2018 CURSO DE DESARROLLO DE APLICACIONES CON TECNOLOGÍA WEB &nbsp; - &nbsp; I. Garrote",
        'numeros' => $datos,
        'resultado' => $resultado,
    ];
}

function actionModa() {
    $datos = $_REQUEST['numeros'];
    /*
      Comprobar si es array o string
     */
    if (gettype($datos) == "string") {
        $datos = explode(",", $datos);
        echo var_dump($datos);
    }
    $resultado = 0;
    foreach ($datos as $valor) {
        if ($valor < $resultado) {
            break;
        } else {
            $resultado = $valor;
        }
    }
    /*
      Convertir los nº en un string
     */
    $datos = implode(",", $datos);
    echo var_dump($datos);
    return [
        'titulo' => "Operaciones matemáticas",
        'content' => 'resultado.php',
        'mensaje' => 'La moda es: ',
        "pie" => "© 2018 CURSO DE DESARROLLO DE APLICACIONES CON TECNOLOGÍA WEB &nbsp; - &nbsp; I. Garrote",
        'numeros' => $datos,
        'resultado' => $resultado,
    ];
}

function actionMediana() {
    $datos = $_REQUEST['numeros'];
    /*
      Comprobar si es array o string
     */
    if (gettype($datos) == "string") {
        $datos = explode(",", $datos);
        echo var_dump($datos);
    }
    $resultado = 0;
    foreach ($datos as $v) {
        $resultado += $v;
    }
    $resultado /= count($datos);
    /*
      Convertir los nº en un string
     */
    $datos = implode(",", $datos);
    echo var_dump($datos);
    return [
        'titulo' => "Operaciones matemáticas",
        'content' => 'resultado.php',
        'mensaje' => 'La mediana es: ',
        "pie" => "© 2018 CURSO DE DESARROLLO DE APLICACIONES CON TECNOLOGÍA WEB &nbsp; - &nbsp; I. Garrote",
        'numeros' => $datos,
        'resultado' => $resultado,
    ];
}

function actionDesviacion() {
    $datos = $_REQUEST['numeros'];
    /*
      Comprobar si es array o string
     */
    if (gettype($datos) == "string") {
        $datos = explode(",", $datos);
        echo var_dump($datos);
    }
    /*$resultado = 0;
    $media = 0;
    foreach ($datos as $v) {
        $media += $v;
    }
    $media /= count($datos);*/
    
    $resultado = 0;
    for ($i = 0; $i < count($datos); $i++) {
        $resultado += $datos[$i];
    }
    $media = $resultado / count($datos);
    $sum2 = 0;
    for ($i = 0; $i < count($datos); $i++) {
        $sum2 += ($datos[$i] - $media) * ($datos[$i] - $media);
    }
    $vari = $sum2 / count($datos);
    $sq = sqrt($vari);
    /*
      Convertir los nº en un string
     */
    $datos = implode(",", $datos);
    echo var_dump($datos);
    return [
        'titulo' => "Operaciones matemáticas",
        'content' => 'resultado.php',
        'mensaje' => 'La desviación aritmética es: ',
        "pie" => "© 2018 CURSO DE DESARROLLO DE APLICACIONES CON TECNOLOGÍA WEB &nbsp; - &nbsp; I. Garrote",
        'numeros' => $datos,
        'resultado' => $resultado,
    ];
}

if (isset($_REQUEST["boton"])) {
    switch ($_REQUEST['boton']) {
        case 'media':
            $datos = actionMedia();
            break;
        case 'moda':
            $datos = actionModa();
            break;
        case 'mediana':
            $datos = actionMediana();
            break;
        case 'desviacion':
            $datos = actionDesviacion();
            break;

        default:
            $datos = actionIndex();
            break;
    }
} else {
    $datos = actionIndex();
}

if (isset($_REQUEST["view"])) {
    $datos["content"] = $_REQUEST["view"] . ".php";
}
    

