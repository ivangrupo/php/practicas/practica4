<!doctype html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title><?= $datos["titulo"] ?></title>
    </head>
    <style>
        #wrapper{
            display: block;
            text-align: center;
            font-size: 1.5em;
        }
        div, label, imput, button, a{
            margin: 5px;
            text-decoration: none;
        }
        h3{
            color: red;
            margin: 20px 100px;
        }
    </style>
    <body>

        <div id="wrapper">

            <h1><?= $datos["titulo"]; ?></h1>
            <div>
                <h3><a href="index.php">FORMULARIO</a></h3>
            </div>
            <form method="get">
                <?php
                include $datos["content"];
                ?>
            </form>

        </div>

    </body>
</html>

