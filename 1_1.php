<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php
        
            $numeros=[3,4,5,2];
            
            asort($numeros);
            
            $posicion=0;
            $ord_numeros=1;
            
            foreach ($numeros as $indice => $valores) {
                echo "$valores ";
                if($indice!=$posicion){
                    $ord_numeros=0;
                }
                $posicion++;
            }
            
            // Es lo mismo que if ($ord_numeros==1)
            
            if ($ord_numeros) {
                echo "Los datos estaban ordenados";
            }else{
                echo "Los datos no estaban ordenados";
            }
            
        ?>
    </body>
</html>
