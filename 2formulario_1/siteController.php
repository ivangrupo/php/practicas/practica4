<?php
        function  actionResultado(){
            $frase =$_REQUEST["frase"];
            
            $contador=[];
            $contador["a"]=substr_count(strtolower($frase), "a");
            $contador["e"]=substr_count(strtolower($frase), "e");
            $contador["i"]=substr_count(strtolower($frase), "i");
            $contador["o"]=substr_count(strtolower($frase), "o");
            $contador["u"]=substr_count(strtolower($frase), "u");
            
            return [
                "titulo"=>"Calcular el número de vocales",
                "content"=>"vocales.php",
                "curso"=>"Año 2018",
                "pie"=>"Resultados de la aplicación",
                "nVocales"=>$contador,
                "frase"=>$frase,
            ];
        }
        
        function  actionResultado1(){
            $frase =$_REQUEST["frase"];
            
            $contador=[];
            $contador["b"]=substr_count(strtolower($frase), "b");
            $contador["c"]=substr_count(strtolower($frase), "c");
            $contador["d"]=substr_count(strtolower($frase), "d");
            $contador["f"]=substr_count(strtolower($frase), "f");
            $contador["g"]=substr_count(strtolower($frase), "g");
            
            return [
                "titulo"=>"Calcular el número de vocales",
                "content"=>"vocales.php",
                "curso"=>"Año 2018",
                "pie"=>"Resultados de la aplicacion",
                "nVocales"=>$contador,
                "frase"=>$frase,
            ];
        }
        
        function actionIndex(){
            return [
                "titulo"=>"Calcular el número de vocales",
                "content"=>"formulario.php",
                "curso"=>"Año 2018",
                "pie"=>"Pagina principal de la aplicación",
                "mensaje"=>"Introduce una frase",
            ];
        }

        
        if (isset($_REQUEST["vocales"])) {
           $datos=actionResultado();
        }elseif(isset($_REQUEST["consonantes"])){
            $datos=actionResultado1();
        }else{
            $datos=actionIndex();
        }


