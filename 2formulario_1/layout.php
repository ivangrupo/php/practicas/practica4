<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title><?= $datos["titulo"]?></title>
    </head>
    <body>
        <h1><?= $datos["titulo"]; ?></h1>
        <h2><?= $datos["curso"]; ?></h2>
        <form method="get">
        <ul>
            <li><button>Introducir frase</button></li><br>
            <li><button name="vocales">Listar vocales</button></li><br>
            <li><button name="consonantes">Listar consonantes</button></li><br>
        </ul>
        
        <?php
            include $datos["content"];
        ?>
        </form>
        <div><?= $datos["pie"]; ?></div>
    </body>
</html>

