<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Formulario</title>
    </head>
    <body>
        <!--<form action="2.php"--> <form method="get" accept-charset="utf-8">
            <br>
            <div>
                <label for="numero1">Número 1 &nbsp;</label>
                <input type="number" name="numero[]" id="numero1" value="2">   <!-- <*?= $num["n1"] ?> -->
            </div>
            <br>
            <div>
                <label for="numero2">Número 2 &nbsp;</label>
                <input type="number" name="numero[]" id="numero2" value="3">
            </div>
            <br>
            <div>
                <label for="numero3">Número 3 &nbsp;</label>
                <input type="number" name="numero[]" id="numero3" value="4">
            </div>
            <br>
            <div>
                <button name="imprimir">IMPRIMIR</button>
            </div>
        </form>
        <?php
            if(isset($_REQUEST["imprimir"])){
                
                $numeros=$_REQUEST["numero"];
                
                $ord_numeros=$numeros;

                sort($ord_numeros);

                foreach ($ord_numeros as $valores) {
                    echo "$valores ";
                }

                if ($ord_numeros==$numeros) {
                    echo "Los datos estaban ordenados";
                }else{
                    echo "Los datos no estaban ordenados";
                }
            }
        ?>
    </body>
</html>
