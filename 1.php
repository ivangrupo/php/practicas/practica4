<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php
        
            $numeros=[3,4,5];
            
            $ord_numeros=$numeros;
            
            sort($ord_numeros);
            
            foreach ($ord_numeros as $valores) {
                echo "$valores ";
            }
            
            if ($ord_numeros==$numeros) {
                echo "Los datos estaban ordenados";
            }else{
                echo "Los datos no estaban ordenados";
            }
            
        ?>
    </body>
</html>
